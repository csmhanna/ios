//
//  AuthRequests.swift
//  Acceptance Tests
//
//  Created by Halcyon Mobile on 08/01/2019.
//

import Foundation

// MARK: - Log in

extension Request.Auth {

    struct Login: RequestRepresentable {
        let email: String
        let password: String

        var suffix: String = API.Path.login
        var method: HTTPMethod = .post

        var parameters: Parameters? {
            return  [
                API.Param.email: email,
                API.Param.password: password
            ]
        }
    }
}

// MARK: - Sign up

extension Request.Auth {

//    If there's no parameter change
//    The following can be used

    struct SignUp: RequestRepresentable {
        let email: String
        let password: String

        let suffix: String = API.Path.signup
        let method: HTTPMethod = .post

        var parameters: Parameters? {
            return [
                API.Param.email: email,
                API.Param.password: password
            ]
        }
    }
}
