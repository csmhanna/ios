//
//  URLs.swift
//  Acceptance Tests
//
//  Created by Halcyon Mobile on 07/02/2019.
//

import Foundation

enum URLs {
    static let testUrl          = URL(string: "https://test.com")
}

enum App {
    static let iTunesId         = 0
}
