//
//  Notifications.swift
//  Acceptance Tests
//
//  Created by Halcyon Mobile on 07/02/2019.
//

import Foundation

extension Notification.Name {
    static let didUpdateSomething = Notification.Name("didUpdateSomething")
}
