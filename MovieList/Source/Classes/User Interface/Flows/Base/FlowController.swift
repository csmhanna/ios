//
//  FlowController.swift
//  project-skeleton
//
//  Created by Halcyon Mobile on 5/17/17.
//  Copyright © 2017 HalcyonMobile. All rights reserved.
//

import UIKit

// INFO:
/* ---

 FlowController is the basic t
 var firstScreen: UIViewController
 
 func start() {
 <#code#>
 }
 
 func finish() {
 <#code#>
 }
 ype for a regular flowcontroller
 
 Has 3 type of presentation possibility, definable on init:
 - present       - start flow by modal presentation
 - push          - start flow as a subflow of an existing navigation flow, pushing new screens in the same navigationcontroller

 flow is started with start(), finished with finish()
 Subclasses need to implement the start(), finish() functions and set firstScreen variable
  - mainViewController - this will be the container vc: tabbar, navigation controller
  - firstScreen - here is the defined and returned the first screen of the flow. makes sense mostly for navigation stacks, where the result is used as the root VC of the new navigationcontroller
 
 If a child flow is started with a parent flow it will be added to the `childFlows` array, otherwise the object will be deallocated
--- */

// MARK: -

protocol FlowController: AnyObject {

    var childFlows: [FlowController] { get set }

    var mainViewController: UIViewController? { get }

    func start()
    func finish()
}

class BaseFlowController: FlowController, FlowControllerDelegate {

    weak var delegate: FlowControllerDelegate?

    var childFlows: [FlowController] = []

    private(set) var parentFlow: FlowController?

    var firstScreen: UIViewController {
        assertionFailure("Set rootViewController!")
        return UIViewController()
    }

    var mainViewController: UIViewController? {
        return firstScreen
    }

    // MARK: - Lifecycle

    required init(from parent: FlowController? = nil) {
        parentFlow = parent
        if let flow = parentFlow as? BaseFlowController {
            delegate = flow
        }
    }

    func start() {
        addToParent()
    }

    func finish() {
        delegate?.didFinish(on: self)
    }

    // MARK: - Helpers

    /**
     Add `self` to the parent flow.
     */
    private func addToParent() {
        parentFlow?.addChild(flow: self)
    }

    /**
     Removes `self` from the parent flow.
     */
    private func removeFromParent() {
        parentFlow?.removeChild(flow: self)
    }

    func didFinish(on flowController: FlowController) {
        removeFromParent()
    }
}

extension FlowController {

    /**
     Add `self` to the parent flow.
     */
    func addChild(flow: FlowController) {
        childFlows.append(flow)
    }

    /**
     Removes `self` from the parent flow.
     */
    func removeChild(flow: FlowController) {
        print(self, " - ", flow)
        if let idx = childFlows.firstIndex(where: { $0 === flow }) {
            childFlows.remove(at: idx)
        }
    }
}
