//
//  LoginViewController.swift
//  Workeepr
//
//  Created by Hanna Kovacs on 27/10/2020.
//

import UIKit

private enum LayoutValues {
    static let illustrationViewWidth: CGFloat = 300
    static let illustrationViewHeight: CGFloat = 200
}

final class LandingViewController: UIViewController {

    // MARK: - Private properties

    private let illustrationView = IllustrationView()
    private let titleLabel = UILabel()
    private let actionButton = UIButton()

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        initConstraints()
    }

    // MARK: - UI

    private func initUI() {
        view.backgroundColor = Asset.Colors.brownLight.color
        view.addSubview(illustrationView)

        titleLabel.text = L10n.landingTitle
        titleLabel.textAlignment = .center
        titleLabel.textColor = .brown
        view.addSubview(titleLabel)
    }

    private func initConstraints() {
        illustrationView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false

        let constraints = [
            illustrationView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            illustrationView.topAnchor.constraint(equalTo: view.topAnchor, constant: .padding6x),
            illustrationView.widthAnchor.constraint(equalToConstant: LayoutValues.illustrationViewWidth),
            illustrationView.heightAnchor.constraint(equalToConstant: LayoutValues.illustrationViewHeight),

            titleLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: .padding2x),
            titleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.padding2x)
        ]
        NSLayoutConstraint.activate(constraints)
    }
}
